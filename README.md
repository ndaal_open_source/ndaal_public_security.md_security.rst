# ndaal_public_security.md_security.rst

This repository contains our versions of [SECURITY.md](./SECURITY.md) and [SECURITY.rst](./SECURITY.rst).  
These contain security procedures and general policies for the ndaal open source projects given under <https://gitlab.com/ndaal_open_source/>.
